# react-medium-editor-loyall
Customized version for Loyall. Completely based on react-medium-editor

React wrapper for [medium-editor](https://github.com/daviferreira/medium-editor)

For better rich text editing with react, check [draft-js](http://draftjs.org/).

### Demo
[http://wangzuo.github.io/react-medium-editor](http://wangzuo.github.io/react-medium-editor)
### Installation
``` sh
npm install react-medium-editor-loyall --save
```
### Usage
``` javascript
// load theme styles with webpack
require('medium-editor/dist/css/medium-editor.css');
require('medium-editor/dist/css/themes/default.css');

// ES module
import React, { Component } from 'react';
import Editor from '../lib/editor';


class App extends Component {
  
  getInitialState() {
    return {
      text: 'Etiam Fringilla Magna Quam'
    };
  }

  render() {
    return (
      <div className="app">
        <h1>react-medium-editor</h1>
        <h3>Html content</h3>
        <div>{this.state.text}</div>

        <h3>Editor #1 (&lt;pre&gt; tag)</h3>
        <Editor
          tag="pre"
          className="editor"
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          text={this.state.text}
          onChange={this.handleChange}
          options={{toolbar: {buttons: ['bold', 'italic', 'underline']}}}
        />

        <h3>Editor #2</h3>
        <Editor
          text={this.state.text}
          onChange={this.handleChange}
        />

        <h3>Editor #3 (editing disabled)</h3>
        <p>Useful for using the toolbar with customized buttons/actions</p>
        <Editor
          style={{ outline: 'dotted 1px', padding: 10 }}
          text={this.state.text}
          options={{disableEditing: true, toolbar: false }}
        />
      </div>
    );
  }

  handleFocus() {
    console.log('onFocus');
  }

  handleBlur() {
    console.log('onBlur');
  }

  handleChange(text, medium) {
    this.setState({
      text: text
    });

    console.log('medium', medium);
  }
}
export default App;
```
### License
MIT
